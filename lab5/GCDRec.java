import java.util.Scanner;

public class GCDRec{
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter your first value:");
		int value1 = reader.nextInt();
		System.out.println("Enter your second value:");
		int value2 = reader.nextInt();
		reader.close();
		int divisor = gcd(value1 > value2 ? value1 : value2, value1 > value2 ? value2:value1);
	
		System.out.println("GCD Loop10 :"  + divisor);
	
	}
	private static int gcd(int value1, int value2) {
		if(value2 == 0)
			return value1;
		return gcd(value2, value1 % value2);
	}
}